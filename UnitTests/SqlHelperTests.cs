﻿using NUnit.Framework;
using Moq;
using PVCodebase.Data;

namespace UnitTests
{
    [TestFixture]
    public class SqlHelperTests
    {
        [Test]
        public void SqlHelper_Instance_Created()
        {
            string connectionString = "foo";
            var o = new SqlHelper(connectionString);
            
            Assert.AreEqual(connectionString, o.ConnectionString);
        }

        [Test]
        public void Mock_test()
        {
            // arrange
            var mock = new Mock<IHelper>();
            mock.Setup(foo => foo.Count()).Returns(5);

            // act
            int i = mock.Object.Count();

            // assert
            Assert.AreEqual(5, i);
        }
    }
}
