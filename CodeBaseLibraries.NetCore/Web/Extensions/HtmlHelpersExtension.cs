﻿using System.IO;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;

namespace CodeBaseLibraries.NetCore.Web.Extensions
{
    public static partial class HtmlHelpersExtension
    {
        /// <summary>
        /// Renderes HTNL-content from the specified file
        /// </summary>
        /// <param name="html"></param>
        /// <param name="Path">Path to the file</param>
        /// <returns></returns>
        public static HtmlString FromFile(this IHtmlHelper html, string Path)
        {
            var env = html.ViewContext.HttpContext.RequestServices.GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;
            return new HtmlString(File.ReadAllText(System.IO.Path.Combine(env.WebRootPath, Path)));
        }
    }
}
