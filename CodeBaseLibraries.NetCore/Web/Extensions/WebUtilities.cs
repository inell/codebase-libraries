﻿using System;
using System.Net;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace CodeBaseLibraries.NetCore.Web.Extensions
{
    public static class WebUtilities
    {
        /// <summary>
        /// Strips Html-tags from the string value
        /// </summary>
        /// <param name="input">String to strip Html-tags</param>
        /// <returns></returns>
        public static string StripHTML(string input) => input != null ? Regex.Replace(input, "<.*?>", String.Empty) : null;

        /// <summary>
        /// Converts a string that has been encoded for transmission in a URL into a decoded string
        /// <see cref="WebUtility"/>
        /// </summary>
        /// <param name="encodedValue">A URL-encoded string to decode</param>
        /// <returns>A decoded string</returns>
        public static string Decode(string encodedValue) => WebUtility.UrlDecode(encodedValue);

        /// <summary>
        /// Converts a text string into a URL-encoded string
        /// <see cref="WebUtility"/>
        /// </summary>
        /// <param name="value">The text to URL-encode</param>
        /// <returns>A URL-encoded string</returns>
        public static string Encode(string value) => WebUtility.UrlEncode(value);

        /// <summary>
        /// Converts a string that has been encoded for transmission in a URL into a decoded string and strips Html-tag from it
        /// </summary>
        /// <param name="input">String to decode and strip Html-tags</param>
        /// <returns></returns>
        public static string DecodeAndStripHTML(string input) => StripHTML(Decode(input));
    }
}
