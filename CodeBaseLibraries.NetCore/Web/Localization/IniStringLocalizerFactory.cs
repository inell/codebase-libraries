﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration.Ini;

namespace CodeBaseLibraries.NetCore.Web.Localization
{
    /// <summary>
    /// Represents a factory that creates IStringLocalizer instances (for View etc.)
    /// </summary>
    public class IniStringLocalizerFactory : IStringLocalizerFactory
    {
        string localizationPath;
        CultureInfo defaultCulture;

        public IniStringLocalizerFactory(string LocalizationPath, CultureInfo DefaultCulture)
        {
            this.localizationPath = LocalizationPath;
            this.defaultCulture = DefaultCulture;
        }

        /// <summary>
        /// Creates an <see cref="IStringLocalizer"/> using the Assembly and FullName of the specified Type
        /// </summary>
        /// <param name="resourceSource">The Type</param>
        /// <returns></returns>
        public IStringLocalizer Create(Type resourceSource) => new IniStringLocalizer(this.localizationPath, this.defaultCulture);

        /// <summary>
        /// Creates an <see cref="IStringLocalizer"/>
        /// </summary>
        /// <param name="baseName">The base name of the resource to load strings from</param>
        /// <param name="location">The location to load resources from</param>
        /// <returns></returns>
        public IStringLocalizer Create(string baseName, string location) => new IniStringLocalizer(this.localizationPath, this.defaultCulture);
    }
}
