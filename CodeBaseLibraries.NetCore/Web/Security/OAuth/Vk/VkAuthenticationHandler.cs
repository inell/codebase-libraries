﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace CodeBaseLibraries.NetCore.Web.Security.OAuth.Vk
{
    public class VkAuthenticationHandler : OAuthHandler<VkAuthenticationOptions>
    {
        /// <summary>
        /// Default contructor with passed to the base <see cref="OAuthHandler"/> arguments
        /// </summary>
        /// <param name="options"></param>
        /// <param name="logger"></param>
        /// <param name="encoder"></param>
        /// <param name="clock"></param>
        public VkAuthenticationHandler(IOptionsMonitor<VkAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) 
            : base(options, logger, encoder, clock) { }

        protected override async Task<AuthenticationTicket> CreateTicketAsync(ClaimsIdentity identity, AuthenticationProperties properties, OAuthTokenResponse tokens)
        {
            // Construct base URL for query
            var address = QueryHelpers.AddQueryString(base.Options.UserInformationEndpoint, new Dictionary<string, string> {
                ["access_token"] = tokens.AccessToken,
                ["v"] = !String.IsNullOrEmpty(base.Options.ApiVersion) ? base.Options.ApiVersion : VkAuthenticationDefaults.ApiVersion
            });
            // Specify fields to obtain
            if (base.Options.Fields.Count != 0)
                address = QueryHelpers.AddQueryString(address, "fields", string.Join(",", base.Options.Fields));

            var response = await base.Backchannel.GetAsync(address, base.Context.RequestAborted);
            if (!response.IsSuccessStatusCode)
            {
                Logger.LogError("An error occurred while retrieving the user profile: the remote server returned a {Status} response with the following payload: {Headers} {Body}.",
                    /* Status: */ response.StatusCode,
                    /* Headers: */ response.Headers.ToString(),
                    /* Body: */ await response.Content.ReadAsStringAsync());
                throw new HttpRequestException("An error occurred while retrieving the user profile.");
            }

            var container = JObject.Parse(await response.Content.ReadAsStringAsync());
            var payload = container["response"].First as JObject;

            var context = new OAuthCreatingTicketContext(new ClaimsPrincipal(identity), properties, base.Context, base.Scheme, base.Options, base.Backchannel, tokens, payload);
            context.RunClaimActions(payload);

            await Options.Events.CreatingTicket(context);

            return new AuthenticationTicket(context.Principal, context.Properties, base.Scheme.Name);
        }
    }
}
