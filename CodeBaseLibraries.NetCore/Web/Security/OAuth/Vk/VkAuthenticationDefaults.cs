﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;

namespace CodeBaseLibraries.NetCore.Web.Security.OAuth.Vk
{
    /// <summary>
    /// Default values used by the VK authentication middleware
    /// </summary>
    public static class VkAuthenticationDefaults
    {
        /// <summary>
        /// Default value for <see cref="AuthenticationScheme.Name"/>
        /// </summary>
        public const string AuthenticationScheme = "VK";

        /// <summary>
        /// Default value for <see cref="AuthenticationScheme.DisplayName"/>
        /// </summary>
        public const string DisplayName = "VK";

        /// <summary>
        /// Default value for <see cref="AuthenticationSchemeOptions.ClaimsIssuer"/>
        /// </summary>
        public const string Issuer = "VK";

        /// <summary>
        /// Default API version
        /// See https://vk.com/dev/versions for more information
        /// </summary>
        public const string ApiVersion = "5.92";

    }
}
