﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace  CodeBaseLibraries.NetCore.Web.Security.OAuth.Vk
{
    /// <summary>
    /// Extension methods to add Vkontakte authentication capabilities to an HTTP application pipeline.
    /// </summary>
    public static class VkontakteAuthenticationExtensions
    {
        /// <summary>
        /// Adds <see cref="VkAuthenticationHandler"/> to the specified
        /// <see cref="AuthenticationBuilder"/>, which enables Vkontakte authentication capabilities
        /// </summary>
        /// <param name="builder">The authentication builder</param>
        /// <returns>The <see cref="AuthenticationBuilder"/></returns>
        public static AuthenticationBuilder AddVk(this AuthenticationBuilder builder)
        {
            return builder.AddVk(VkAuthenticationDefaults.AuthenticationScheme, options => { });
        }

        /// <summary>
        /// Adds <see cref="VkAuthenticationHandler"/> to the specified
        /// <see cref="AuthenticationBuilder"/>, which enables Vkontakte authentication capabilities
        /// </summary>
        /// <param name="builder">The authentication builder</param>
        /// <param name="configuration">The delegate used to configure the Vkontakte options</param>
        /// <returns>The <see cref="AuthenticationBuilder"/></returns>
        public static AuthenticationBuilder AddVkontakte(
                this AuthenticationBuilder builder,
            Action<VkAuthenticationOptions> configuration)
        {
            return builder.AddVk(VkAuthenticationDefaults.AuthenticationScheme, configuration);
        }

        /// <summary>
        /// Adds <see cref="VkontakteAuthenticationHandler"/> to the specified
        /// <see cref="AuthenticationBuilder"/>, which enables Vkontakte authentication capabilities
        /// </summary>
        /// <param name="builder">The authentication builder</param>
        /// <param name="scheme">The authentication scheme associated with this instance</param>
        /// <param name="configuration">The delegate used to configure the Vkontakte options</param>
        /// <returns>The <see cref="AuthenticationBuilder"/></returns>
        public static AuthenticationBuilder AddVk(
                this AuthenticationBuilder builder, string scheme,
            Action<VkAuthenticationOptions> configuration)
        {
            return builder.AddVk(scheme, VkAuthenticationDefaults.DisplayName, configuration);
        }

        /// <summary>
        /// Adds <see cref="VkAuthenticationHandler"/> to the specified
        /// <see cref="AuthenticationBuilder"/>, which enables Vkontakte authentication capabilities
        /// </summary>
        /// <param name="builder">The authentication builder</param>
        /// <param name="scheme">The authentication scheme associated with this instance</param>
        /// <param name="caption">The optional display name associated with this instance</param>
        /// <param name="configuration">The delegate used to configure the Vkontakte options</param>
        /// <returns>The <see cref="AuthenticationBuilder"/></returns>
        public static AuthenticationBuilder AddVk(
                this AuthenticationBuilder builder,
                string scheme, string caption,
                Action<VkAuthenticationOptions> configuration)
        {
            return builder.AddOAuth<VkAuthenticationOptions, VkAuthenticationHandler>(scheme, caption, configuration);
        }
    }
    
}
