﻿using System;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CodeBaseLibraries.NetCore.Web.HttpSession
{
    /// <summary>
    /// <see cref="ISession"/> extension class
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        /// Sets an <see cref="Object"/> to the <see cref="ISession"/>
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <param name="value"><see cref="Object"/> value</param>
        public static void SetObject(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value, 
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
             ));
        }

        /// <summary>
        /// Gets an <see cref="Object"/> of <typeparamref name="T"/> from the <see cref="ISession"/>
        /// </summary>
        /// <typeparam name="T">Type of the retrieving object</typeparam>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <returns>Retrieved <see cref="Object"/> of <typeparamref name="T"/></returns>
        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// Sets a <see cref="Boolean"/> to the <see cref="ISession"/>
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <param name="value"><see cref="Boolean"/> value</param>
        public static void SetBoolean(this ISession session, string key, bool value)
        {
            session.Set(key, BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Gets an <see cref="Boolean"/> of <typeparamref name="T"/> from the <see cref="ISession"/>
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <returns>Retrieved <see cref="Boolean"/></returns>
        public static bool? GetBoolean(this ISession session, string key)
        {
            var data = session.Get(key);
            if (data == null)
            {
                return null;
            }
            return BitConverter.ToBoolean(data, 0);
        }

        /// <summary>
        /// Sets a <see cref="Double"/> to the <see cref="ISession"/>
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <param name="value"><see cref="Double"/> value</param>
        public static void SetDouble(this ISession session, string key, double value)
        {
            session.Set(key, BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Gets an <see cref="Double"/> of <typeparamref name="T"/> from the <see cref="ISession"/>
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key">Key value</param>
        /// <returns>Retrieved <see cref="Double"/></returns>
        public static double? GetDouble(this ISession session, string key)
        {
            var data = session.Get(key);
            if (data == null)
            {
                return null;
            }
            return BitConverter.ToDouble(data, 0);
        }
    }
}
