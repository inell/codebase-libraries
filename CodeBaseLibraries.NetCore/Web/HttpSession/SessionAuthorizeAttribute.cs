﻿using System;
using System.Linq;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace CodeBaseLibraries.NetCore.Web.HttpSession
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SessionAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// Session handler delegate to process HTTP session data.
        /// If returns "false" - user Sign Out occurs
        /// </summary>
        public static Func<HttpContext, bool?> SessionHandlerDelegate;

        public async void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;

            if (!user.Identity.IsAuthenticated)
            {
                // it isn't needed to set unauthorized result 
                // as the base class already requires the user to be authenticated
                // this also makes redirect to a login page work properly
                // context.Result = new UnauthorizedResult();
                return;
            }

            // Handle roles
            if (this.Roles != null)
            {
                var isAuthorized = true;
                foreach (var role in this.Roles.Split(",", StringSplitOptions.RemoveEmptyEntries))
                    isAuthorized &= user.IsInRole(role);

                if (!isAuthorized)
                {
                    context.Result = new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                    return;
                }
            }

            // Handle policies
            if (this.Policy != null)
            {
                var authorizationService = context.HttpContext.RequestServices.GetService(typeof(IAuthorizationService)) as IAuthorizationService;
                var authorizationResult = await authorizationService.AuthorizeAsync(context.HttpContext.User, this.Policy);
                if (!authorizationResult.Succeeded)
                {
                    context.Result = new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                    return;
                }
            }

            var attributeIndex = context.Filters.Where(t => t.GetType() == this.GetType()).ToList().IndexOf(this);
            // If index of this attribute in Filters-list above zero, this attribute has been launched previously in the request
            if (attributeIndex == 0)
            {
                var isAuthorizedHttpSession = SessionHandlerDelegate?.Invoke(context.HttpContext);
                if (isAuthorizedHttpSession.HasValue && !isAuthorizedHttpSession.Value)
                {
                    await context.HttpContext.SignOutAsync();

                    // When you work with authentication, the expected behaviour when you call .Signout() is that .Signout() will return false. But nope!
                    // You need to assign new user with the new "GenericPrincipal" with "GenericIdentity" is assigned to user in current context.
                    // New identity with empty name and null as authentication type.
                    context.HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                    return;
                }
            }
        }
    }
}
