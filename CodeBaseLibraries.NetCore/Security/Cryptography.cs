﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace CodeBaseLibraries.NetCore.Security
{
    /// <summary>
    /// Provides basic cryptography methods
    /// </summary>
    public static class Cryptography
    {
        /// <summary>
        /// Compute SHA 256 hash
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string ComputeSha256Hash(string RawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(RawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        /// <summary>
        /// Encrypt string with Rijndael (AES)
        /// </summary>
        /// <param name="RawString">String to encrypt</param>
        /// <param name="Key">Symmetric authKey</param>
        /// <returns></returns>
        public static string EncryptAes(string RawString, string Key)
        {
            string encrypted;
            SymmetricAlgorithm sa = Rijndael.Create();
            using (ICryptoTransform ct = sa.CreateEncryptor((new PasswordDeriveBytes(Key, null)).GetBytes(16), new byte[16]))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                    {
                        byte[] value = Encoding.UTF8.GetBytes(RawString);
                        cs.Write(value, 0, value.Length);
                        cs.FlushFinalBlock();
                        encrypted = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }

            return encrypted;
        }

        /// <summary>
        /// Decrypt string with Rijndael (AES)
        /// </summary>
        /// <param name="CryptedString">String to decrypt</param>
        /// <param name="Key">Symmetric authKey</param>
        /// <returns></returns>
        public static string DecryptAes(string CryptedString, string Key)
        {
            string decrypted;
            try
            {
                byte[] value = Convert.FromBase64String(CryptedString);

                SymmetricAlgorithm sa = Rijndael.Create();
                using (ICryptoTransform ct = sa.CreateDecryptor((new PasswordDeriveBytes(Key, null)).GetBytes(16), new byte[16]))
                {
                    using (MemoryStream ms = new MemoryStream(value))
                    {
                        using (CryptoStream Cs = new CryptoStream(ms, ct, CryptoStreamMode.Read))
                        {
                            using (StreamReader Sr = new StreamReader(Cs))
                            {
                                decrypted = Sr.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (CryptographicException)
            {
                return null;
            }

            return decrypted;
        }
    }
}
