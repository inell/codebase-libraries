﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;

namespace CodeBaseLibraries.NetCore.Logging
{
    public class FileLogger : ILogger
    {
        object _lock = new object();
        protected readonly string filePath;
        protected readonly LogLevel logLevel;

        public FileLogger(string Path, LogLevel LogLevel)
        {
            this.filePath = Path;
            this.logLevel = LogLevel;
        }

        public IDisposable BeginScope<TState>(TState state) => null;

        public bool IsEnabled(LogLevel logLevel) => logLevel >= this.logLevel;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!this.IsEnabled(logLevel) || formatter == null)
                return;

            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("{0:dd.MM.yyyy HH:mm:ss.fff} {1}", DateTime.Now, formatter(state, exception)));
            if (exception != null)
            {
                sb.AppendLine($"{exception.GetType().Name}: {exception.Message}");
                sb.AppendLine(exception.StackTrace);
            }

            lock (_lock)
            {
                File.AppendAllText(filePath, sb.ToString() + Environment.NewLine);
            }
        }
    }
}
