﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Logging;


namespace CodeBaseLibraries.NetCore.Logging
{
    public class FileLoggerProvider : ILoggerProvider
    {
        protected readonly string basePath;
        protected readonly LogLevel logLevel;

        public FileLoggerProvider(string BasePath, LogLevel LogLevel = LogLevel.Information)
        {
            this.basePath = BasePath;
            this.logLevel = LogLevel;

            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
        }

        Dictionary<string, ILogger> instances = new Dictionary<string, ILogger>();

        public ILogger CreateLogger(string CategoryName)
        {
            if(!this.instances.ContainsKey(CategoryName))
                this.instances.Add(CategoryName, new FileLogger(Path.Combine(this.basePath, CategoryName) + ".log", this.logLevel));

            return this.instances[CategoryName];
        }

        public void Dispose() { }
    }
}
