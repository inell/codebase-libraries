﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Json;

namespace CodeBaseLibraries.Serialization
{
    /// <summary>
    /// JsonSerialization Helper
    /// </summary>
    public class JsonSerializationHelper
    {
        /// <summary>
        /// Generates an instance of object from file with serialized data
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="Path">Path to file contains serialized data</param>
        /// <returns>An instance of object</returns>
        public static T DeserializeFromFile<T>(string Path)
        {
            T result = default(T);

            byte[] data = File.ReadAllBytes(Path);
            string Value = convertDate(Encoding.UTF8.GetString(data));

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(Value)))
            {
                result = loadFromStream<T>(ms);
            }

            return result;
        }

        /// <summary>
        /// Generates an instance of object from string serialized data
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="Value">String serialized data</param>
        /// <returns>An instance of object</returns>
        public static T DeserializeFromString<T>(string Value)
        {
            T result = default(T);

            Value = convertDate(Value);

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(Value)))
            {
                result = loadFromStream<T>(ms);
            }
            return result;
        }

        /// <summary>
        /// Saves serialization object data to the file
        /// </summary>
        /// <param name="Instance">Instance of object</param>
        /// <param name="Path">Path to new file</param>
        public static void SerializeToFile(object Instance, string Path)
        {
            string Value = SerializeToString(Instance);

            using (StreamWriter sw = File.CreateText(Path))
            {
                sw.Write(Value);
            }
        }

        /// <summary>
        /// Serialized into string object 
        /// </summary>
        /// <param name="Instance">Instance of object</param>
        /// <returns>String serialized data</returns>
        public static string SerializeToString(object Instance)
        {
            string result = null;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(Instance.GetType());
            
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, Instance);
                result = Encoding.UTF8.GetString(ms.ToArray());
            }

            return stringifyDate(result);
        }

        /// <summary>
        /// Deserializes an instance of object from stream
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="stream"></param>
        /// <returns>An instance of object</returns>
        private static T loadFromStream<T>(MemoryStream stream)
        {
            T result = default(T);
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));

            try
            {
                result = (T)serializer.ReadObject(stream);
            }
            catch (InvalidOperationException ex)
            {
                //InnerException containts data
                throw ex.InnerException;
            }
            finally
            {
                stream.Close();
            }

            return result;
        }

        /// <summary>
        /// Convert "\/Date(1319266795390+0800)\/" String as "yyyy-MM-dd HH:mm:ss"
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        static string stringifyDate(string value)
        {
            //Replace Json Date String
            string p = @"\\/Date\((\d+)\+\d+\)\\/";
            MatchEvaluator matchEvaluator = new MatchEvaluator(m =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(m.Groups[1].Value)).ToLocalTime();
                return dt.ToString("yyyy-MM-dd HH:mm:ss");
            });
            return new Regex(p).Replace(value, matchEvaluator);
        }

        /// <summary>
        /// Convert "yyyy-MM-dd HH:mm:ss" String as "\/Date(1319266795390+0800)\/"
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        static string convertDate(string value)
        {
            string p = @"\d{4}-\d{2}-\d{2}(\s\d{2}:\d{2}:\d{2})?";
            MatchEvaluator matchEvaluator = new MatchEvaluator(m =>
            {
                DateTime dt = DateTime.Parse(m.Groups[0].Value).ToUniversalTime();
                TimeSpan ts = dt - DateTime.Parse("1970-01-01");
                return String.Format("\\/Date({0}+0800)\\/", ts.TotalMilliseconds);
            });
            return new Regex(p).Replace(value, matchEvaluator);
        }
    }
}
